const {env4} = require('../dist/main')

function main() {
  env4()
    .define({
      vars: {
        APP_NAME: {
          desc: 'foo'
        }
      }
    })
    .validate({
      APP_NAME: ''
    })
    .exitIfIssues()

  console.log('App...')
}

main()
