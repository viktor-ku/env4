interface Var {
  required?: boolean
  desc?: string
}

interface Config {
  vars: Record<string, Var>
}

interface Issue {
  env: string
  desc: string
  issue: string
}

class Session {
  config: Config
  private issues: Issue[] = []

  define(config: Config): this {
    this.config = config
    return this
  }

  validate(input: Record<string, string | undefined>): this {
    if (!this.config)
      throw new Error('poop')

    for (const one of Object.entries(this.config.vars)) {
      const config = {
        key: one[0],
        varSettings: {
          required: true,
          ...one[1],
        }
      }

      const actual = input[config.key]

      if (config.varSettings.required && !actual) {
        this.issues.push({
          env: config.key,
          desc: config.varSettings.desc || '',
          issue: 'Required, but not specified'
        })
      }
    }

    return this
  }

  exitIfIssues() {
    if (!this.issues.length)
      return

    console.table(this.issues)
    process.exit(1)
  }
}

export function env4() {
  const session = new Session()

  return session
}
